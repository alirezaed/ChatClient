﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatClient.Models
{
    public class User
    {

        public string PhoneNo { get; set; }
        public Guid Reference { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SecurityCode { get; set; }
    }
}
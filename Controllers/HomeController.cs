﻿using ChatClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ChatClient.Controllers
{
    public class HomeController : Controller
    {
        HttpClient _client;
        private JavaScriptSerializer jss = new JavaScriptSerializer();
        public HomeController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("http://localhost:7666/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        // GET: /Home/


        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User model)
        {
            TempData["model"] = model;

            if (ModelState.IsValid)
            {
                var response = _client.GetAsync("api/Authentication/UserExists/" + model.PhoneNo).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    if (responseString.Equals("false"))
                    {
                        response = _client.GetAsync("api/Authentication/SaveNewUser/" + model.PhoneNo).Result;
                        responseString = response.Content.ReadAsStringAsync().Result;
                        this.ViewBag.PhoneNo = model.PhoneNo;
                        if (responseString.Equals("true"))
                            return RedirectToAction("SignUp");
                    }
                    else
                    {
                        return RedirectToAction("InputPassword");
                    }
                }
            }
            return RedirectToAction("Login");
        }
        [HttpPost]
        public ActionResult InputPassword(User model)
        {
            User std = (User)TempData["model"];
            model.PhoneNo = std.PhoneNo;
            TempData["model"] = model;
            if (ModelState.IsValid)
            {
                var response = _client.GetAsync(string.Format("api/Authentication/IsPasswordCorrect/{0}/{1}", model.PhoneNo, model.Password)).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;

                    if (responseString.Equals("true"))
                    {
                        LoginUser(std.PhoneNo);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("RestorePassword");
                    }
                }
            }
            return RedirectToAction("Login");
        }

        private void LoginUser(string phoneNo)
        {
            //var response = _client.GetAsync(string.Format("api/Authentication/GetUser/{0}", phoneNo)).Result;
            //string userobj = response.Content.ReadAsStringAsync().Result;
            var response = _client.GetAsync(string.Format("api/Authentication/GenerateToken/{0}", phoneNo)).Result;
            string token = response.Content.ReadAsStringAsync().Result;
            
            //Session["UserReference"] = userobj.Split(':')[3];
            //Session["UserName"] = userobj.Split(':')[1];
            Session["Token"] = token;
        }

        [HttpGet]
        public ActionResult InputPassword()
        {
            User std = (User)TempData["model"];
            TempData["model"] = std;
            return View(std);
        }
        [HttpPost]
        public ActionResult SignUp(User model)
        {
            User std = (User)TempData["model"];
            model.PhoneNo = std.PhoneNo;
            TempData["model"] = model;
            if (ModelState.IsValid)
            {
                var response = _client.GetAsync(string.Format("api/Authentication/IsSecurityCodeCorrect/{0}/{1}", model.PhoneNo, model.SecurityCode)).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    if (responseString.Equals("true"))
                    {
                        response = _client.GetAsync(string.Format("api/Authentication/UpdateUserInfo/{0}/{1}/{2}", model.PhoneNo, model.Password, model.UserName)).Result;
                        responseString = response.Content.ReadAsStringAsync().Result;
                        if (responseString.Equals("true"))
                        {
                            LoginUser(std.PhoneNo);
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        this.ViewBag.ErrorMessage = "Invalid Security Code";
                        return View();
                    }
                }
            }
            
            return View();
        }
        public ActionResult SignUp()
        {
            User std = (User)TempData["model"];
            TempData["model"] = std;
            return View(std);
        }

        public ActionResult RestorePassword()
        {
            return View();
        }
    }
}

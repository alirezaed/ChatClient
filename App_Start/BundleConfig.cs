﻿using System.Web;
using System.Web.Optimization;

namespace ChatClient
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
						//bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						//						"~/Scripts/jquery-1.10.2.js",
						//						"~/Scripts/jquery-1.10.2-vsdoc.js",
						//						"~/Scripts/jquery-1.10.2.min.js",
						//						"~/Scripts/jquery-1.10.2.min.map"
						//						));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
             "~/Scripts/bootstrap.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
								"~/Content/css/bootstrap.css",
								"~/Content/css/bootstrap-theme.css"
                ));

        }
    }
}